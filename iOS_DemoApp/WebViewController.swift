//
//  WebViewController.swift
//  iOS_DemoApp
//
//  Created by rsaeki on 2020/04/10.
//

import UIKit
import WebKit

class WebViewController: UIViewController {
    
   
    @IBOutlet weak var WebView: WKWebView!
    var topPadding:CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "WebView"
        
        let url = URL(string: "https://www.sonix.asia")
        let request = URLRequest(url: url!)
        
        WebView.load(request)
        
    }

}
