//
//  ViewController.swift
//  iOS_DemoApp
//
//  Created by rsaeki on 2020/04/10.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource  {
    
    //追加②
    let List = ["Picker", "Maps", "Video","WebView","ViewPager","Form"]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "DemoApp"
    }
    
    //追加③ セルの個数を指定するデリゲートメソッド（必須）
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return List.count
    }
    
    //追加④ セルに値を設定するデータソースメソッド（必須）
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // セルを取得する
        let cell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        // セルに表示する値を設定する
        cell.textLabel!.text = List[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(indexPath.row == 0){
            // セルの選択を解除
                   tableView.deselectRow(at: indexPath, animated: true)
            
                   // 別の画面に遷移
                   performSegue(withIdentifier: "toPicker", sender: nil)
        }
        else if(indexPath.row == 1){
            // セルの選択を解除
                   tableView.deselectRow(at: indexPath, animated: true)
            
                   // 別の画面に遷移
                   performSegue(withIdentifier: "toMaps", sender: nil)
        }
        else if(indexPath.row == 2){
            // セルの選択を解除
                   tableView.deselectRow(at: indexPath, animated: true)
            
                   // 別の画面に遷移
                   performSegue(withIdentifier: "toVideo", sender: nil)
        }
        else if(indexPath.row == 3){
            // セルの選択を解除
                   tableView.deselectRow(at: indexPath, animated: true)
            
                   // 別の画面に遷移
                   performSegue(withIdentifier: "toWebView", sender: nil)
        }
        else if(indexPath.row == 4){
            // セルの選択を解除
                   tableView.deselectRow(at: indexPath, animated: true)
            
                   // 別の画面に遷移
                   performSegue(withIdentifier: "toViewPager", sender: nil)
        }
        else if(indexPath.row == 5){
            // セルの選択を解除
                   tableView.deselectRow(at: indexPath, animated: true)
            
                   // 別の画面に遷移
                   performSegue(withIdentifier: "toForm", sender: nil)
        }

    }
}

